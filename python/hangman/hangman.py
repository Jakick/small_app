# Name: Shyaka Laniesse
# Last Modified: 07-11-16

from random import randrange
from string import *
import os
import sys

# Uncomment the next statement and place the directory where
# the Python module and words.txt files are located
# os.chdir("C:/...")

# Import hangman words

WORDLIST_FILENAME = "words.txt"

def load_words():
    """
    Returns a list of valid words. Words are strings of lowercase letters.
    
    Depending on the size of the word list, this function may
    take a while to finish.
    """
    print("Loading word list from file...")
    # inFile: file
    inFile = open(WORDLIST_FILENAME, 'rt')
    # line: string
    line = inFile.readline()
    # wordlist: list of strings
    wordlist = line.split()
    print("  ", len(wordlist), "words loaded.")
    print('Enter play_hangman() to play a game of hangman!')
    return wordlist

# load the dictionary (not a Python dictionary, just a list
# words in the English dictionary) and point to it with 
# the words_dict variable 
words_dict = load_words()

# get_word: void -> str
# Purpose: expects nothing
#    returns a word rendomly selected from the file "words.txt" 
def get_word():
    """
    Returns a random word from the word list
    """
    word = words_dict[randrange(0, len(words_dict))]
    return word

# print_hangman_image: int -> void
# Purpose: expects an integer representing the number of incorrect guesses
#    during the Hangman game, and returns nothing
# Side effect: prints to the screen a drawing of the hangman based on the
# number of incorrect guesses
def print_hangman_image(mistakes):
  """Prints out a gallows image for hangman.
  The image printed depends on the number of mistakes (0-6)."""

  if mistakes <= 0:
    print('''
        __________
        |    |       
        |
        |
        |
        |
        |_________
        ''')

  elif mistakes == 1:
    print('''
        __________
        |    |       
        |    O
        |
        |
        |
        |_________
        ''')
  elif mistakes == 2:
    print('''
        __________
        |    |       
        |    O
        |    |
        |    |
        |
        |_________
        ''')
  elif mistakes == 3:
    print('''
        __________
        |    |       
        |    O
        |   /|
        |    |
        |
        |_________
        ''')
  elif mistakes == 4:
    print('''
        __________
        |    |       
        |    O
        |   /|\\
        |    |
        |   
        |_________
        ''')
  elif mistakes == 5:
    print('''
        __________
        |    |       
        |    O
        |   /|\\
        |    |
        |   /
        |_________
        ''')
  else: # mistakes >= 6
    print('''
        __________
        |    |       
        |    O
        |   /|\\
        |    |
        |   / \\
        |_HANGED!_

        ''')
  
# -----------------------------------

# CONSTANTS
MAX_GUESSES = 6

# GLOBAL VARIABLES 
secret_word = get_word()
letters_guessed = []
letters_entered = []

# word_guessed: void -> bool
# purpose:  expects nothing
#     returns True if the word in the global variable "secret_word"
#     has been guessed, based on the "letters_guessed" list
#     and returns False otherwise
def word_guessed():
    '''
    Returns True if the player has successfully guessed the word,
    and False otherwise.
    '''
    global secret_word
    global letters_guessed

    e = 0
    for letter in secret_word:
        if letter not in letters_guessed:
            e += 1
    if e > 0:
        return False
    else:
        return True

# print_guessed: void -> void
# purpose:  expects nothing and returns nothing
# side effect: prints to the screen (on one line) the part of the secret word
# that has been guessed 
def print_guessed():
    '''
    Prints out the characters you have guessed in the secret word so far
    '''
    global secret_word
    global letters_guessed

    ret_string = ""
    for letter in secret_word:
        if letter in letters_guessed:
            ret_string += letter + " "
        else:
            ret_string += "_ "
    return ret_string

# play_hangman: void -> void
# purpose: expects nothing, anf return nothing
# This function should control the playing of the game Hangman by choosing a
# secret word, asking the player for letters to guess, and displaying both
# the Hangman image and displaying how much of the word has been guessed after
# each letter is entered
def play_hangman():
    # Actually play the hangman game
    global secret_word
    global letters_guessed
    global letters_entered
    mistakes_made = 0
    while mistakes_made < 6:
        if word_guessed() == False:
            l = input("Guess a letter: ")
            letters_entered.append(l)
            if l in secret_word and l not in letters_guessed:
                letters_guessed.append(l)
                print_hangman_image(mistakes_made)
                pg = "       " + print_guessed()
                print(pg)
                s = ""
                e = ""
                for i in letters_guessed:
                    s += " " + i
                for i in letters_entered:
                    e += " " + i
                print('       LETTERS GUESSED:' + s)
                print('       LETTERS ENTERED:' + e)
                print("")
                print("")
            else:
                mistakes_made += 1
                print_hangman_image(mistakes_made)
                if mistakes_made >= 6:
                    print("You are Hanged, too many mistakes!!")
                    print("The secret word was: " + secret_word)
                    secret_word  = get_word()
                    letters_guessed = []
                    tagain = input('Try again ? (Y/y for yes, any other for no): ')
                    if tagain == 'y' or tagain == 'Y':
                        play_hangman()
                    else:
                        sys.exit()
                else:
                    pg = "       " + print_guessed()
                    print(pg)
                    s = ""
                    e = ""
                    for i in letters_guessed:
                        s += " " + i
                    for i in letters_entered:
                        e += " " + i
                    print('       LETTERS GUESSED:' + s)
                    print('       LETTERS ENTERED:' + e)
                    print("")
                    print("")
        else:
            print('You won!!')
            secret_word  = get_word()
            letters_guessed = []
            tagain = input('Try again ? (Y/y for yes, any other for no): ')
            if tagain == 'y' or tagain == 'Y':
                play_hangman()
            else:
                sys.exit()

    return None
