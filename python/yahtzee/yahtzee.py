# Shyaka Laniesse
# Last modified: May 02, 2016

import random
import collections

SIDES = 6
NUM_DICE = 5
NUM_ROLLS = 3
NUM_TESTS = 50

# Die() class and methods
class Die():
    def __init__(self, number_of_sides):
        self.number_of_sides = number_of_sides
        
    def roll(self):
        self.__rolled_value = random.randint(1, self.number_of_sides)

    def value(self):
        return self.__rolled_value
        
# is_yahtzee: list of Die objects -> boolean
# purpose:  expects a list of Die objects
#    returns True if all values of Die object match (a Yahtzee)
#    returns False otherwise
def is_yahtzee(the_dice):
    result = True
    for i in range(1, NUM_DICE):
        if the_dice[i].value() != the_dice[i-1].value():
            result = False
    return result

# print_dice: list of Die objects -> nothing
# purpose: expects a list of Die objects
#    returns nothing
# side effect: prints to screen the values on the dice
def print_dice(the_dice):
    print("The dice values are: {}".format([the_dice[i].value() for i in range(0,NUM_DICE)]))

# best_value_to_keep: list of Die objects -> int
# purpose: expects a list of Die objects
#    examines the values of the dice, and determines which value is most common
#    returns that most common value
# Examples: the_dice values of [1, 5, 4, 5, 5] will return 5
#           the_dice values of [2, 6, 2, 6, 4] will return 6

def best_value_to_keep(the_dice):
    # Create a counter dictionary of the values in the_dice
    dice_counter = collections.Counter(the_dice[i].value() for i in range(0, NUM_DICE))
    
    # Look for the highest number of dice that match each other
    dice_that_match = max(dice_counter.values())
    
    # Find which value on the dice (that is, which key in the counter)
    # corresponds to dice_that_match
    for i in list(dice_counter.keys()):
        if dice_counter[i] == dice_that_match:
            value_to_keep = i
            
    # value_to_keep now is the most commonly found value on the dice
    return value_to_keep

# play_yahtzee: list of Die objects -> boolean
# purpose: expects a list of Dir objects
#     plays Yahtzee with the dice to try to get a Yahtzee
#     returns True if a Yahtzee is made or False otherwise
# side effect: prints to the screen the values of the dice
#     (using print_dice) after each roll, and a single-line
#     "Yahtzee" or "No Yahtzee" message at the end

def play_yahtzee(the_dice):
    for dice in the_dice:
        dice.roll()
    n = 0
    b_value = best_value_to_keep(the_dice)
    while n < NUM_ROLLS-1:        
        for dice in the_dice:
            if dice.value() != b_value:
                dice.roll()
        n = n + 1
        print_dice(my_dice)
        # because the best value can change we need to check after each roll
        # for example we have two '4' so the best value is '4'
        # but then we have three '5' with the two '4', then the best value is 5
        b_value = best_value_to_keep(the_dice)
    if (is_yahtzee(the_dice)):
        return True
    else:
        return False

# *** The "main" code ***
    
# First, create a list called my_dice of Die objects

my_dice = []
for i in range(0, NUM_DICE):
    my_dice.append(Die(SIDES))
    
# Then, either call play_yahtzee(my_dice) once, or write a
# loop to call play_yahtzee(my_dice) multiple times, based
# on NUM_TESTS, and print the results

numb = 0
numy = 0
for i in range(0, NUM_TESTS):
    if (play_yahtzee(my_dice)):
        print("********** YAHTZEEEEEEEEEEEE *********")
        numy = numy + 1
    else:
        print("NO Yahtzee")
    numb = numb + 1
print("The number of test conducted are {} and the number of yahtzee is {}".format(numb, numy))
    
